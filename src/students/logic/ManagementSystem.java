/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package students.logic;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;

/**
 *
 * @author Владимир
 */

public class ManagementSystem {
    
    private List<Group> groups;
    private Collection<Student> students;
    
    // Для шаблона Singletone статическая переменная
    private static ManagementSystem instance;

    public ManagementSystem() {
        loadGroups();
        loadStudents();
    }
    
    // метод getInstance - проверяет, инициализирована ли статическая
    // переменная (в случае надобности делает это) и возвращает ее    
    public static synchronized ManagementSystem getInstance(){
        if (instance == null){
            instance = new ManagementSystem();
        }
        return instance;
    }
    
   /* public static void main(String[] args) {
        try{
            System.setOut(new PrintStream("out.txt"));
        } catch (FileNotFoundException ex){
            ex.printStackTrace();
            return;
        }

    //    ManagementSystem ms = ManagementSystem.getInstance();
        
        //Просмотр полного списка групп
        printString("Полный список групп ");
        printString("************************");        
        List<Group> allGroups = ms.getGroups();
                for (Group gi : allGroups){
                    printString(gi);
                }
                printString();
        //Просмотр полного списка студентов
        printString("Полный список студентов");
        printString("*****************************");
        Collection<Student> allStudends = ms.getAllStudents();
            for(Student si : allStudends) {
                printString(si);
            }
            printString();
            
         // Вывод списка студентов по группам
         printString("Список студентов по группам");
         printString("*********************************");
         List<Group> groups = ms.getGroups();
         // Проверяем все группы
            for (Group gi : groups) {
                printString("-----> Группа: " + gi.getNameGroup());
                //Получаем список студентов для конкретной группы
                Collection<Student> students =  ms.getStudentsFromGroup(gi, 2006);
                for (Student si : students){
                    printString(si);
                }
            }
            printString();
            
            //Создаём нового студента и добавляем его в список
            Student s = new Student();
            s.setStudentId(5);
            s.setFirstName("Туко");
            s.setPatronymic("Педрович");
            s.setSurName("Рамирес");
            s.setGender('М');
            Calendar c = Calendar.getInstance();
            c.set(1980, 23, 07);
            s.setDateOfBirth(c.getTime());
            s.setGroupId(1);
            s.setEducationYear(2006);
            printString("Добавление студента: " + s);
            printString("******************************");  
            ms.insertStudent(s);
            printString("------>>  Полный списко студентов после добавления");
            allStudends = ms.getAllStudents();
            for (Student si : allStudends){
                printString(si);    
            }
            printString();
            
            s = new Student();
            s.setStudentId(5);
            s.setFirstName("Туко");
            s.setPatronymic("Педрович");
            s.setSurName("Дамирес");
            s.setGender('М');
            c = Calendar.getInstance();
            c.set(1980, 23, 07);
            s.setDateOfBirth(c.getTime());
            s.setGroupId(1);
            s.setEducationYear(2006);
            printString("Добавление студента: " + s);
            printString("******************************");  
            ms.insertStudent(s);
            printString("------>>  Полный списко студентов после редактирования");
            allStudends = ms.getAllStudents();
            for (Student si : allStudends){
                printString(si);    
            }
            printString();   
            
            // Удалим студента
            printString("Удаление студента: " + s);
            printString("****************************");
            ms.deleteStudent(s);
            printString("Полный список студентов после удаления: " + s);    
            allStudends = ms.getAllStudents();
            for(Student si : allStudends){
                printString(si);
            }
            printString();  
            
            //Переводим студентов из одной группы  в другую. У нас две группы.
            Group g1 = groups.get(0);
            Group g2 = groups.get(1);            
            printString("Перевод студентов из первой группы во вторую");   
            printString("****************************");
            ms.moveStudentsFromGroup(g1, 2006, g2, 2007);
            printString("Полный список студентов после перевода ");    
            allStudends = ms.getAllStudents();
            for (Student si : allStudends){
                printString(si);    
            }
            printString();
            
            //Удаляем студентов из группы
            printString("Удаление студента из группы: " + g2 + " в 2006 году");    
            printString("****************************");
            ms.revomveStudentsFromGroup(g2, 2006);
            printString("Полный список студентов после удаления ");    
            allStudends = ms.getAllStudents();
            for (Iterator i = allStudends.iterator(); i.hasNext();) {
                printString(i.next());    
        } 
               printString();       
    }
*/     
//Метод создает две группы и помещает их в коллекцию для групп
          public void loadGroups(){
              // Проверяем создан ли список
              if (groups == null){
                  groups = new ArrayList<Group>();
              }
              else{
                  groups.clear();
              }
              Group g = null;
              
              g = new Group();
              g.setGroupId(1);
              g.setNameGroup("Первая");
              g.setCurator("Доктор Борменталь");
              g.setSpeciality("Создание собачек из человеков");
              groups.add(g);
              
              g = new Group();
              g.setGroupId(2);
              g.setNameGroup("Вторая");
              g.setCurator("Профессор Преображенский");
              g.setSpeciality("Создание человеков из собачек");
              groups.add(g);
          }
          
          
          //Метод создает студентов и автоматически помещает их в коллекцию
          public void loadStudents(){
              if(students == null){
                  students = new TreeSet<Student>();
              } else
                {
                  students.clear();
                }
              Student s = null;
              Calendar c = Calendar.getInstance();
              
              //Вторая группа
              s = new Student();
              s.setStudentId(1);
              s.setFirstName("Иван");
              s.setPatronymic("Сергеевич");
              s.setSurName("Степанов");
              s.setGender('M');
              c.set(1990, 3, 20);
              s.setDateOfBirth(c.getTime());
              s.setGroupId(2);
              s.setEducationYear(2006);
              students.add(s);
              
              s = new Student();
              s.setStudentId(2);
              s.setFirstName("Наталья");
              s.setPatronymic("Андреевна");
              s.setSurName("Чичикова");
              s.setGender('Ж');
              c.set(1990, 6, 10);
              s.setDateOfBirth(c.getTime());
              s.setGroupId(2);
              s.setEducationYear(2006);
              students.add(s);

              //Первая группа
              s = new Student();
              s.setStudentId(3);
              s.setFirstName("Верноника");
              s.setPatronymic("Сергеевна");
              s.setSurName("Петрова");
              s.setGender('Ж');
              c.set(1990, 2, 22);
              s.setDateOfBirth(c.getTime());
              s.setGroupId(1);
              s.setEducationYear(2006);
              students.add(s);
              
              s = new Student();
              s.setStudentId(4);
              s.setFirstName("Сергей");
              s.setPatronymic("Андреевич");
              s.setSurName("Сидоров");
              s.setGender('М');
              c.set(1990, 7, 15);
              s.setDateOfBirth(c.getTime());
              s.setGroupId(2);
              s.setEducationYear(2006);
              students.add(s);              
          }
          
          //Получить список групп
          public List<Group> getGroups(){
              return groups;
          }
          
          //Получить список всех студентов
          public Collection<Student> getAllStudents(){
              return students;
          }
 
          //  Получить список студентов для определенной группы
           public Collection<Student> getStudentsFromGroup(Group group, int year){
              Collection<Student> l = new TreeSet<Student>();
              for(Student si : students){
                  if(si.getGroupId() == group.getGroupId() && si.getEducationYear() == year) {
                        l.add(si);
                  }
              }
              return  l;
           }   
           
            // Перевести студентов из одной группы с одним годом обучения в другую группу с другим годом обучения          
           public void moveStudentsFromGroup(Group oldGroup, int oldYear, Group newGroup, int newYear){
              for(Student si : students){
                  if(si.getGroupId() == oldGroup.getGroupId() && si.getEducationYear() == oldYear){
                      si.setGroupId(newGroup.getGroupId());
                      si.setEducationYear(newYear);
                  }
              }
          }
          
          // Удалить всех студентов из определенной группы
          public void revomveStudentsFromGroup (Group group, int year){
        // Мы создадим новый список студентов БЕЗ тех, кого мы хотим удалить.
        // Возможно не самый интересный вариант. Можно было бы продемонстрировать
        // более элегантный метод, но он требует погрузиться в коллекции более глубоко
        // Здесь мы не ставим себе такую цель              
            Collection<Student> tmp = new TreeSet<Student>();
            for (Student si : students){    
                if (si.getGroupId() != group.getGroupId() || si.getEducationYear() != year)
                {
                    tmp.add(si);
                }
            }
            students = tmp;
          }
          
          //Добавить студента
          public  void insertStudent (Student student){
                students.add(student);
          }
          
          // Обновить данные о студенте
          public void updateStudent (Student student){
              // ищем нужного студента по id и изменить поля
                    Student updStudent = null;
                    for(Student si : students){
                        if(si.getStudentId() == student.getStudentId()){
                            // Вот этот студент, запомнили его и прекратили поиск
                            updStudent = si;
                            break;
                        }
                    }
                    updStudent.setFirstName(student.getFirstName());
                    updStudent.setPatronymic(student.getPatronymic());
                    updStudent.setSurName(student.getSurName());
                    updStudent.setGender(student.getGender());
                    updStudent.setDateOfBirth(student.getDateOfBirth());
                    updStudent.setGroupId(student.getGroupId());
                    updStudent.setEducationYear(student.getEducationYear());
          }
          
          //Удалить студента
          public void deleteStudent (Student student){
               // Находим по id нужного студента и удаляем его
               Student delStudent = null;
               for(Student si : students){
                   if(si.getStudentId() == student.getStudentId()){
                       //Нашли нужного студента и запомнили его
                       delStudent = si;
                       break;
                   }
               }
               students.remove(delStudent);
          }
          
          // Кодировка
          public static void printString (Object s){
                //System.out.println(s.toString());
                try{
                    System.out.println(new String(s.toString().getBytes("windows-1251"), "windows-1251"));
                } catch (UnsupportedEncodingException exception) 
                    {
                        exception.printStackTrace();
                    }
          }
          
            public static void printString(){
                System.out.println(""); 
            }
    }
    


